# Exposing predictive analytics through natural language interfaces

## Data 

[RITA airlines dataset (2009 ASA challenge)](http://stat-computing.org/dataexpo/2009/the-data.html)

## Run

        python preprocess.py
        python predict.py (<INTEGER>)
        gf <verbalizations.gfs

File paths, relevant columns, as well as filters and buckets are specified in `config.json`.

