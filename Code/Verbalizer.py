import json

from config import getBucket


domain    = 'ASAAnalytics'
languages = ['Eng']


class Verbalizer:

      def __init__(self,config):

          self.pgf = None

          self.plain_exact = None
          self.plain_vague = None
          self.likelihood_exact = None
          self.likelihood_vague = None
          self.confidence_exact = None
          self.confidence_vague = None

          self.config = config


      def transform(self,prediction):

          if    prediction.target == 'DepDelay':
                kind = 'asa_Departure'
          elif  prediction.target == 'ArrDelay':
                kind = 'asa_Arrival'
          else: kind = 'owl_Thing'

          # plain prediction

          # zero value
          if str(prediction.predicted) == '0' or str(prediction.predicted) == 'NO':
          	 self.plain_exact = 'say (plain (predict_not (apply1 '+ kind +' (existential_closure '+ kind +' xsd_integer asa_'+ prediction.target +') (The '+ kind +' (class2predicate '+ kind +')))))'
             return

          # negative value
          if '-' in str(prediction.predicted):
          	 # TODO: AST for "... will be early"
          	 pass 

          # construct exact and vague value

          if prediction.isBucket:

          	 # exact value is range min(bucket) max(bucket)
             value_exact = self._bucketRangeToValue(prediction.target,prediction.predicted,"Minutes")
             # vague value is predicted 
             value_vague = self._bucketToValue(prediction.predicted,"Minutes")

          else:

          	 # exact value is predicted + unit
             if int(prediction.predicted) == 1: 
                   unit  = "Minute"
             else: unit  = "Minutes"
             value_exact = "(value_int_unit " + str(int(prediction.predicted)) + " " + unit + ")"
             # value_vague depends on in which bucket predicted is
             buck = self.getBucket(prediction.target,prediction.predicted)
             if buck is not None:
             	   value_vague = self._bucketToValue(buck,"Minutes")
             else: value_vague = "(several Minutes)"

          # build AST

          theTarget  = "(The "+kind+" (class2predicate "+kind+"))"
          predicate  = lambda e,v: "(predict (apply2 "+kind+" xsd_integer asa_"+prediction.target+" " + e + " " + v + "))"

          self.plain_exact = '(say ' + predicate(theTarget,value_exact) + ')'
          self.plain_vague = '(say ' + predicate(theTarget,value_vague) + ')'

          if prediction.likelihood is not None:
             if    prediction.likelihood < 0.4:
                   measure = 'Slight'
             elif  prediction.likelihood > 0.8:
                   measure = 'High'
             else: measure = 'Medium'
             self.likelihood_exact = '(propability_exact ' + predicate(theTarget,value_exact) + ' (value_int_unit ' + str(prediction.likelihood*100) + ' Percent))'
             self.likelihood_vague = '(probability_descr ' + predicate(theTarget,value_vague) + ' ' + measure + ')'

          if prediction.confidence is not None:
             if    prediction.confidence < 0.4:
                   attitude = 'Think_NotSure'
             elif  prediction.confidence > 0.8:
                   attitude = 'Expect'
             else: attitude = 'Think_Sure'
             self.confidence_exact = '(attitude ' + predicate(theTarget,value_exact) + ' ' + attitude + ')'
             self.confidence_vague = '(attitude ' + predicate(theTarget,value_vague) + ' ' + attitude + ')'


      def verbalize(self):

          with open('verbalizations.gfs','w') as script:
               script.write('i grammars/' + domain + '.pgf')
               for ast in [self.plain_exact,self.plain_vague,self.likelihood_exact,self.likelihood_vague,self.confidence_exact,self.confidence_vague]:
                   if ast is not None: script.write('\nl -all '+ast)
               

      def show(self):

      	  print "\nVerbalizations:"

      	  print "\n* Plain prediction (exact): "
      	  print self.plain_exact 

      	  print "\n* Plain prediction (descriptive): "
      	  print self.plain_vague

          print "\n* Probabilistic prediction (exact): "
          print self.likelihood_exact

          print "\n* Probabilistic prediction (descriptive): "
          print self.likelihood_vague

          print "\n* Confidence (exact): "
          print self.likelihood_exact

          print "\n* Confidence (descriptive): "
          print self.likelihood_vague


      def getBucket(self,target,value):

          if self.config.has_key('buckets'):
           for b in self.config['buckets']:
               if b['column'] == target: 
                  for k,v in b['names'].items():
                      if v.has_key('enum') and value in v['enum']:
                         return k
                      if     v.has_key('min') and value >= v['min']:
                         if (v.has_key('max') and value <= v['max']) or not v.has_key('max'):
                             return k
                      elif   v.has_key('max') and value >= v['max']:
                         if (v.has_key('min') and value <= v['min']) or not v.has_key('min'):
                             return k

      def _bucketToValue(self,buck,unit): 

          if buck in ['LOW']:
             return "(a_few " + unit + ")"
          if buck in ['MEDIUM']: 
             return "(several " + unit + ")"
          if buck in ['HIGH']:
             return "(many " + unit + ")" # TODO not in grammar yet
          
          # fallback
          return "(some " + unit + ")"

      def _bucketRangeToValue(self,target,bucket,unit):

          if self.config.has_key('buckets'):
           for b in self.config['buckets']:
               if b['column'] == target: 
                  if b['names'].has_key(bucket):
                     d = b['names'][bucket]
                     if d.has_key('min') and d.has_key('max'):
                        return "(range_int_unit " + str(int(d['min'])) + " " + str(int(d['max'])) + " " + unit + ")"
                     if d.has_key('min'):
                     	return "(at_least " + str(int(d['min'])) + " " + unit + ")"
                     if d.has_key('max'):
                     	return "(at_most "  + str(int(d['max'])) + " " + unit + ")"
                     if d.has_key('enum'):
                     	return "(one_of " + str(d['enum']) + ")" # TODO not in grammar yet

          # fallback 
          return "<some exact value>"
