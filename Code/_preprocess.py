import itertools
import csv
import random


# Global TODO's
# ------------------
# Check that itertools is used correctly and 90MB is truely streamed
# Acces rows as dict instead of list, in for instance the lambda's


class CSVProcessor:

	infile = ''
	outfile = ''
	
	derived_column_headers=[]
	derived_columns = []
	
	output_columns = []
	
	filters=[]
	
	headers=[]
	
	def __init__(self, infile, outfile):
		self.infile = infile
		self.outfile
	
	def addRowFilter(self, filter):
		self.filters.append(filter)
	
	def addDerivedColumn(self, header, derivation):
		self.derived_column_headers.append(header);
		self.derived_columns.append(derivation)
		
	def setOutputColumns(outputColumns)
		self.output_columns = outputColumns
	
	
	#include filters and derived columns here
	def process(self):
		outrows = 0
		with open(self.infile, 'rt') as csvfile:	    	
			datareader = csv.DictReader(csvfile, delimiter=',')
			for row in datareader:
				# Check that all filters match
				if(all([filter(row) for filter in self.filters])):				
					# Add derived columns here
					for x in range(len(self.derived_columns)):
						row[self.derived_column_headers[x]] = self.derived_columns[x](row);
					# Output
					print (row)
					outrows = outrows + 1
		
		print 'Number of Output Rows', outrows
        

#### SET 1
##

c = CSVProcessor('../Data/first100.csv', '../Data/out.csv')

c.setOutputColumns(['ArrDelay', 'ArrDelayClass', 'DepDelay', 'DepDelayClass']);

## Use filters to create relevant subsets of data file

## Filter based on content
#c.addRowFilter((lambda x: x['FlightNum'] == '1016'))
c.addRowFilter((lambda x: int(x['ArrDelay']) > 0))

## Add a 1 in 1000 filter to reduce across the board?
#c.addRowFilter((lambda x: random.random() < 0.1))


# Use derivedColumns for Discretization of ranges
def ArrDelayClass(row):
	minutes = int(row['ArrDelay'])
	if(minutes > 15 and minutes < 30):
		return "MEDIUM"
	elif(minutes >= 30):
		return "HIGH"
	else:
		return "LOW"
		
def DepDelayClass(row):
	minutes = int(row['DepDelay'])
	if(minutes > 15 and minutes < 30):
		return "MEDIUM"
	elif(minutes >= 30):
		return "HIGH"
	else:
		return "LOW"

c.addDerivedColumn('ArrDelayClass', ArrDelayClass);
c.addDerivedColumn('DepDelayClass', DepDelayClass);


c.process()
