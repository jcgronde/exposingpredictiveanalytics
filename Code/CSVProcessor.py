import itertools
import csv
import json


class CSVProcessor:

    def __init__(self,renaming=None):

        self.infile   = None
        self.outfile  = None
        self.refile   = None

        self.renaming = renaming

        self.filters  = []
        self.columns  = []
        self.buckets  = []

    def setInFile(self,path):
        self.infile   = path

    def setOutFile(self,path):
        self.outfile  = path

    def setRenamingFile(self,path):
        self.refile   = path

    def setRenaming(self,renaming):
        self.renaming = renaming

    def addRowFilter(self,filter):
        self.filters.append(filter)

    def addColumns(self,columns):
        self.columns += columns

    def addBucket(self,name,ranges,otherwise='INVALID'):
        self.buckets.append(dict(name=name,ranges=ranges,otherwise=otherwise))

    def reset(self):
        self.filters = []
        self.buckets = []


    def process(self):

        outrows    = 0
        runningVar = 1

        if self.renaming is None:
              renaming = dict(NA=0, NEGATIVE=1)
        else: renaming = self.renaming

        with open(self.infile,'r') as f_in:
             with open(self.outfile,'w') as f_out:
                  
                  reader = csv.DictReader(f_in) 
                  
                  # columns to keep
                  if self.columns: columns = [ c for c in reader.fieldnames if c in self.columns ]
                  else:            columns = reader.fieldnames
                  
                  writer = csv.DictWriter(f_out,columns+[ bucket['name']+'BUCKET' for bucket in self.buckets ])
                  writer.writeheader()

                  for row in reader:
                      # check that all filters match
                      if all([filter(row) for filter in self.filters]):
                      	 # keep only specified columns
                         row = { k: row[k] for k in columns }
                         # process buckets
                         for bucket in self.buckets:
                             # add new derived column
                             new_column = bucket['name']+'BUCKET'
                             # if value is NA, leave it
                             if row[bucket['name']] == 'NA': 
                                row[new_column] = renaming['NA']
                                continue
                             # if value is negative, use predefined bucket
                             if '-' in unicode(row[bucket['name']]):
                                row[new_column] = renaming['NEGATIVE']
                                continue
                             # otherwise replace by bucketed value
                             value   = int(row[bucket['name']]) 
                             matched = False
                             for k,r in bucket['ranges'].items():
                                 if value in r: 
                                    row[new_column] = k
                                    matched = True
                                    continue
                             if not matched: 
                                    row[new_column] = bucket['otherwise']
                         # turn all strings into numbers
                         for k,v in row.items():
                             if not unicode(v).replace('-','').isnumeric():
                                if v in renaming.keys():
                                   row[k] = renaming[v]
                                else:
                                   runningVar += 1
                                   row[k]      = runningVar
                                   renaming[v] = runningVar
                         # write row
                         writer.writerow(row)
                         outrows += 1

        # write renaming to a file
        self.renaming = renaming
        open(self.refile,'w').write(json.dumps({ v:k for k,v in renaming.items() }))

        print str(outrows) + ' rows written to ' + self.outfile
