import sys

from Predictor  import *
from Verbalizer import *


config_file = 'config.json'


def run(index=None):

    config = json.load(open(config_file))

    for target in config['predict_columns']:

        print "\n------------------------------------------------"
        print "Running with target " + target + "..."
        print "------------------------------------------------"        

        predictor  = Predictor(config['path']+config['target'],target,config['path']+config['renaming'],config)
        verbalizer = Verbalizer(config)

        data_size  = len(predictor.data)

        if index is not None:
           print "\nData point (" + str(index) + " of " + str(data_size) + "):"
           test = predictor.data[index]
        else:
           index = random.randrange(data_size)
           print "\nChoosing random data point (" + str(index) + " of " + str(data_size) + "):"
           test = predictor.data[index]

        print str(test)

        print "\nRegression target:     " + str(predictor.target[index]) 
        print "Classification target: " + str(int(predictor.target_bucket[index])) + " (" + predictor.renaming[str(int(predictor.target_bucket[index]))] + ")"
           
        regression(predictor,index,test,verbalizer)
        classification(predictor,index,test,verbalizer)


def classification(predictor,index,test,verbalizer):

    print "\n---- Classification ----"

    train_data   = predictor.data[:index] + predictor.data[index+1:]
    train_target = predictor.target_bucket[:index] + predictor.target_bucket[index+1:]

    print "\n-- Naive Bayes (Gaussian) \n"
    pipeline(predictor,predictor.bayes,train_data,train_target,test,verbalizer)

    print "\n-- Decision Tree Classifier \n"
    pipeline(predictor,predictor.treeC,train_data,train_target,test,verbalizer)

    print "\n-- SVM Classifier (Linear kernel) \n"
    pipeline(predictor,predictor.svmC,train_data,train_target,test,verbalizer)

def regression(predictor,index,test,verbalizer):

    print "\n---- Regression ----"

    train_data   = predictor.data[:index] + predictor.data[index+1:]
    train_target = predictor.target[:index] + predictor.target[index+1:]

    #print "\n-- Logistic Regression \n"
    #pipeline(predictor,predictor.logR,train_data,train_target,test,verbalizer)

    print "\n-- K-nearest Neighbor Regression (k=3) \n"
    pipeline(predictor,predictor.knn,train_data,train_target,test,verbalizer)


def pipeline(predictor,instance,train_data,train_target,test,verbalizer):

    predictor.prediction.reset()
    predictor.predict(instance,train_data,train_target,test)
    predictor.prediction.show()
    
    verbalizer.transform(predictor.prediction)
    verbalizer.show()

    out = verbalizer.verbalize()



if __name__ == "__main__":
   if len(sys.argv) > 1: 
   	  run(int(sys.argv[1]))
   else: 
   	  run()
