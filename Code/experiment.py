import argparse
from os import listdir
from os.path import isfile, join

import numpy as np
import pylab as pl
import scipy.stats as stats

from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import LinearSVC
from sklearn.neighbors import KNeighborsRegressor
from sklearn.cross_validation import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from sklearn.preprocessing import StandardScaler

class Data:
    def __init__(self, data = [], target = [], labels = []):
        self.data = np.array(data)
        self.target = np.array(target)
        self.labels = labels

def predict(classifier, data):
    return classifier.predict(data)

def train(classifier, data, target):
    classifier.fit(data, target)

def score(classifier, data, target):
    return classifier.score(data, target)

def get_prediction_prob(classifier, data):
    if hasattr(classifier, 'predict_proba'):
        return classifier.predict_proba(data)
    else:
        return None

def load_dataset(file_path, target_label, separator):
    dataset = read_csv(file_path, target_label, separator)
    return dataset

def read_csv(file_path, target_label, separator=",", has_header = True):
    data = []
    target = []
    labels = []
    target_index = 0
    with open(file_path) as f:
        if has_header: 
            labels = f.readline().strip().split(separator)
            target_index = labels.index(target_label)
            labels.remove(target_label)
        for line in f:
            line = line.strip().split (separator)
            target.append(float(line[target_index]))
            data.append([float(x) for x in line[1:target_index]] + [float(y) for y in line[target_index+1:]])
    return Data(data, target, labels)

def running_classifiers(classifiers, classifierNames, data_train, data_test, target_train, target_test):
    for i in xrange(len(classifiers)):
        classifier = classifiers[i]
        
        scalar = StandardScaler()
        X_train_scaled = scalar.fit_transform(data_train, target_train)

        train(classifier, X_train_scaled, target_train)

        X_test_scaled = scalar.transform(data_test)

        #print 'Evaluating the classifier'
        #print 'Accuracy:', score(classifier, data_test, target_test), '\n'
        data_predict = predict(classifier, X_test_scaled)
        print classifierNames[i], "accuracy:", accuracy_score(target_test, data_predict, normalize=True)


def main(args):
    if args.experiment:
        experiment(args)
    elif args.plot:
        plot(args)
    else:
        print 'Loading dataset from', args.path
        data = load_dataset(args.path, args.target, args.separator)
        print 'Loading classifier', args.classifier
        if args.classifier == 'DecisionTree':
            classifier = DecisionTreeClassifier()
        elif args.classifier == 'NaiveBayes':
            classifier = GaussianNB()
        elif args.classifier == 'LogisticRegression':
            classifier = LogisticRegression()
        else:
            print 'Something went wrong. I do not have a classifier.'
            return

        print 'Splitting the data with test size', args.testsize
        data_train, data_test, target_train, target_test = train_test_split(data.data, data.target, test_size=args.testsize)

        print 'Training the classifier'
        train(classifier, data_train, target_train)

        print 'Evaluating the classifier'
        print 'Accuracy:', score(classifier, data_test, target_test)

def experiment(args):
    classifiers = [GaussianNB(),  LinearSVC(), DecisionTreeClassifier()]
    classifierNames = ["Naive Bayes", "Support Vector Machine", "Decision Tree Classifier"]

    regressors = [KNeighborsRegressor(n_neighbors=3,weights='distance')]
    regressorNames = ["K-nearest Neighbors"]

    mypath = "../Data/flights"
    onlyfiles = [ f for f in listdir(mypath) if isfile(join(mypath,f)) and join(mypath,f).endswith(".csv") ]
    for filename in onlyfiles:
        filepath = mypath + '/' + filename
        print "loading", filepath
        data = load_dataset(filepath, "DepDelayBUCKET", args.separator)

        data_train, data_test, target_train, target_test = train_test_split(data.data, data.target, test_size=args.testsize)

        running_classifiers(classifiers, classifierNames, data_train, data_test, target_train, target_test)
  
        data = load_dataset(filepath, args.target, args.separator)

        data_train, data_test, target_train, target_test = train_test_split(data.data, data.target, test_size=args.testsize)

        for i in xrange(len(regressors)):
            classifier = regressors[i]
            scalar = StandardScaler()
            X_train_scaled = scalar.fit_transform(data_train, target_train)

            train(classifier, X_train_scaled, target_train)

            X_test_scaled = scalar.transform(data_test)

            #print 'Evaluating the classifier'
            #print 'Accuracy:', score(classifier, data_test, target_test), '\n'
            data_predict = np.nan_to_num(predict(classifier, X_test_scaled))
            print regressorNames[i], "coefficient-of-determination:", r2_score(target_test, data_predict)

def plot(args):

    data = load_dataset(args.path, args.target, args.separator)
    h = np.sort(data.target)
    pl.subplot(2, 1, 1)
    pl.hist(h, range=[-100,500], bins=50, color='k',alpha=0.5, normed=True)  
    pl.xlabel('DepDelay')
    pl.xticks(np.arange(-100, 500, 100))
    pl.yticks(np.arange(0,0.03,0.005))

    data = load_dataset(args.path, 'ArrDelay', args.separator)
    h = np.sort(data.target)
    pl.subplot(2, 1, 2)
    pl.hist(h, range=[-100,500], bins=50, color='k',alpha=0.5, normed=True)   
    pl.xlabel('ArrDelay')
    pl.xticks(np.arange(-100, 500, 100))
    pl.yticks(np.arange(0,0.03,0.005))
    
    pl.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Exposing Predictive Analytics through Natural Language Dialog experiment.')
    parser.add_argument('--path', type=str, default='../Data/flights/2008_preprocessed_randomSample_withDelayCauses.csv', help='The path to the Flight dataset (default: %(default)s).')
    parser.add_argument('--separator', type=str, default=',', help='The separator used in the source file (default: %(default)s).')
    parser.add_argument('--target', type=str, default='DepDelay', help='The column label that will be used as target (default: %(default)s).')
    parser.add_argument('--classifier', type=str, default='DecisionTree', help='The classifier to be used. Options are: DecisionTree OR NaiveBayes OR LogisticRegression (default: %(default)s).')
    parser.add_argument('--testsize', type=int, default=0.4, help='The relative size of the test set (default: %(default)s).')
    parser.add_argument('--experiment', action='store_true')
    parser.add_argument('--plot', action='store_true')
    args = parser.parse_args()
    main(args)

