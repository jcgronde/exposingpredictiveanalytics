import json
import random
import pandas
import CSVProcessor      as csv
import matplotlib.pyplot as plt
import numpy             as np


config_file = 'config.json'
include_optional_columns = False


def run(random=True):

    config = json.load(open(config_file))

    c = csv.CSVProcessor()
    c.setInFile(config['path']+config['source'])
    c.setOutFile(config['path']+config['target'])
    c.setRenamingFile(config['path']+config['renaming'])

    c.addColumns(config['input_columns'])
    c.addColumns(config['predict_columns'])
    if include_optional_columns and config.has_key('optional_columns'): 
       c.addColumns(config['optional_columns'])

    if config.has_key('buckets'):
     for b in config['buckets']:
         d = dict()
         r = None 
         for k,v in b['names'].items():
             if v.has_key('enum'):
                d[k] = v['enum']
             else:
                if   v.has_key('min') and v.has_key('max'):
                     d[k] = range(v['min'],v['max']+1)
                elif v.has_key('min') or v.has_key('max'): 
                     r = k
         if r is not None:
            c.addBucket(b['column'],d,r)
         else: 
            c.addBucket(b['column'],d)

    if config.has_key('filters'):
     for f in config['filters']:
         if   f.has_key('column') and f.has_key('values'):
              col = f['column']; val = f['values']
              c.addRowFilter((lambda x: x[col] in val))
         elif f.has_key('exclude'):
              c.addRowFilter((lambda x: f['exclude'] not in x.values()))

    c.process()
    plot(c.outfile,config['predict_columns'])

    if random:
    # create random sample for testing
       c.setInFile(c.outfile)
       c.setOutFile(c.outfile.replace('.csv','_random.csv'))

       c.addColumns([ b['column']+'BUCKET' for b in config['buckets'] ])

       c.reset()
       c.addRowFilter((lambda x: random.random() < .01))

       c.process()
       plot(c.outfile,config['predict_columns'])
    


def plot(path,targets):

    data = pandas.read_csv(path)

    data[targets].hist(range=[0,400],color='k',alpha=0.5,bins=30,normed=True)

    plt.xticks(np.arange(0,400,100))
    plt.yticks(np.arange(0,0.04,0.005))
   
    plt.savefig(path.replace('.csv','_histo.png'))


if __name__ == "__main__":
   run(False)
