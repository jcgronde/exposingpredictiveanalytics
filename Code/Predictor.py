import csv
import json
import numpy as np
import random

from sklearn.naive_bayes  import GaussianNB
from sklearn.svm          import LinearSVC
from sklearn.tree         import DecisionTreeClassifier, DecisionTreeRegressor, export_graphviz
from sklearn.neighbors    import KNeighborsRegressor


class Predictor:

      def __init__(self,path,target,renaming,config):

          print "\nTarget:" 
          print "* " + target + "for regression"
          print "* " + target + "BUCKET for classification" 

          self.config = config

          reader = csv.DictReader(open(path,'r'))

          # read data from CSV
          self.data          = []
          self.target        = []
          self.target_bucket = []
          for row in reader:
              self.data.append([ float(row[k]) for k in row.keys() if not k in [target,target+'BUCKET'] ])
              self.target.append(float(row[target])) 
              self.target_bucket.append(float(row[target+'BUCKET']))
          self.columns = reader.fieldnames

          # classifiers
          self.bayes = GaussianNB()
          self.svmC  = LinearSVC()
          self.treeC = DecisionTreeClassifier()

          # regressors
          self.knn   = KNeighborsRegressor(3,weights='distance') # default: 'uniform'
          self.treeR = DecisionTreeRegressor()

          # read renaming from JSON
          self.renaming   = json.load(open(renaming))

          # prediction
          self.prediction = Prediction(target,self.renaming)


      def _isClassifier(self,x):
          return type(x) in [GaussianNB,LinearSVC,DecisionTreeClassifier]
      def _isRegressor(self,x):
          return type(x) in [KNeighborsRegressor,DecisionTreeRegressor]


      def predict(self,classifier,train_data,train_target,test):
          
          ## Train
          classifier.fit(np.array(train_data),np.array(train_target))

          ## Predict
          pred = classifier.predict(test)[0]
          self.prediction.set_predicted(pred)

          if self._isClassifier(classifier): 
             self.prediction.set_isBucket(True)
             pred_key = unicode(self.prediction.predicted).split('.')[0]
             if pred_key in self.renaming.keys():
                self.prediction.set_predicted(self.renaming[pred_key])
                 
          ## Probability

          if   type(classifier) in [GaussianNB,DecisionTreeClassifier]:
               prob = classifier.predict_proba(test)[0]
               index = np.where(classifier.classes_ == pred)[0][0]
               self.prediction.set_likelihood(round(prob[index],2))

          ## Confidence
             
          if   type(classifier) == KNeighborsRegressor:
               distances = classifier.kneighbors(test,3)[0][0]
               normalized_distances = [ d / max(distances) for d in distances ]

               print '...Distance to 3 nearest neighbors: ' + str(distances)
               print '...Normalized (where min=0, max=max(distances)): ' + str(normalized_distances)
               print ''

               self.prediction.set_confidence(round(min(normalized_distances),2))

          elif type(classifier) == LinearSVC:
               # If confidence scores are required, but these do not have to be probabilities, 
               # then it is advisable to set probability=False and use decision_function instead of predict_proba.
               # (http://scikit-learn.org/stable/modules/svm.html)
               prob = classifier.decision_function(test)[0]
               normalized_prob = np.exp(prob) / np.sum(np.exp(prob)) # softmax             
               index = np.where(classifier.classes_ == pred)[0][0]
               self.prediction.set_confidence(round(normalized_prob[index],2))
               
               print '...Confidence values for all classes: ' + str( zip([ self.renaming[str(int(x))] for x in classifier.classes_ ],normalized_prob) )
               print ''

          ## Other algorithm-specific metadata

          if   type(classifier) == DecisionTreeClassifier:

               #tree = classifier.tree_
               #export_graphviz(classifier)
               probs = zip([ self.renaming[str(int(x))] for x in classifier.classes_ ],prob)
               print '...Probabilities of all classes: ' + str(probs)
               fimp = classifier.feature_importances_
               index = fimp.tolist().index(max(fimp))
               print '...Most important feature: ' + str(self.columns[index])
               print '' 


class Prediction:

      def __init__(self,target,renaming):

          self.target     = target
          self.renaming   = renaming

          self.predicted  = None
          self.isBucket   = None
          self.likelihood = None
          self.confidence = None


      def set_predicted(self,value):
          self.predicted  = value

      def set_isBucket(self,boolean):
          self.isBucket   = boolean

      def set_likelihood(self,prob):
          self.likelihood = prob 

      def set_confidence(self,conf):
          self.confidence = conf


      def reset(self):

          self.predicted  = None
          self.isBucket   = None
          self.likelihood = None
          self.confidence = None       


      def show(self):
          
          print "Prediction: " + str(self.predicted)

          if self.likelihood is not None:
             print "Likelihood: " + str(self.likelihood)

          if self.confidence is not None:
             print "Confidence: " + str(self.confidence)

